```mermaid
graph TD
  home(Bestelldetails Ansicht) --> create[Losteile erstellen Knopf wird betätigt]

  create --> questionOwnBadge{Gibt es zu dieser<br>Bestellposition<br>bereits Losteile?}
  questionOwnBadge --> selectBoth(Hinweis-Dialog mit<br>Auswahl zwischen <br><b>Losteilansicht</b> und<br><b>Stückliste</b>)

  style home fill: #7FDBFF, stroke: #7FDBFF
  style selectBoth fill: #FFCCCC, stroke: #FFCCCC
```